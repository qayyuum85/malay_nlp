from django.apps import AppConfig


class BahasadictionaryConfig(AppConfig):
    name = 'bahasaDictionary'
