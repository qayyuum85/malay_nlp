import urllib.request as rq
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import sys,regex as rgx,json, time
from selenium import webdriver
from selenium.common import exceptions

class FacebookAPIImporter:
    'Importing comments based on Facebook User'
    fbPrefixUrl = r'https://graph.facebook.com/v2.12/'
    fbCommentUrl = r'/comments?fields=comment_count%2Cmessage%2Ccreated_time%2Cid'
    fbPostsUrl = r'/posts?'

    def __init__(self , pageUrl, access_token):
        self.pageUrl = pageUrl
        self.access_token = '&access_token=' + access_token

    def createPostURL(self):
        url = self.fbPrefixUrl + self.entityId + self.fbPostsUrl + self.access_token
        return url

    def createCommentURL(self, post_id):
        url = self.fbPrefixUrl + post_id + self.fbCommentUrl + self.access_token
        return url

    def setGroupId(self):
        """Get Facebook group id based on group link"""
        response = rq.urlopen(self.pageUrl)
        if response.getcode() == 200:
            html = response.read()
            soup = BeautifulSoup(html, 'html.parser')
            scripts = soup.find_all('script')
            rgxSearch = rgx.compile(r'entity_id(?:\'|"):(?:\'|")([\d]+)(?:\'|")')
            entityIds = [rgxSearch.search(x.text).group(1) for x in scripts if rgxSearch.search(x.text) is not None]
            self.entityId = entityIds[0]
            print(self.entityId)
        else:
            self.entityId = ""

    def getPostList(self):
        postUrl = self.createPostURL()
        result = self.getItemPerPage(postUrl)
        return result

    def getCommentList(self, post_id):
        comment_url = self.createCommentURL(post_id)
        result = self.getItemPerPage(comment_url)
        return result

    def getItemPerPage(self, url):
        """Import data from Facebook according to the Graph API URL"""
        next_link = None
        response_comment = rq.urlopen(url)
        if response_comment.getcode() == 200:
            decoded_json_comment = json.loads(response_comment.read().decode('utf-8'))
            if 'error' in decoded_json_comment:
                return None
            else:
                data = decoded_json_comment['data']
                if 'paging' in data and 'next' in data['paging']:
                    next_link = data['paging']['next']
                return (data, next_link)
        return None

    def getAllItems(self, start_url):
        all_comments = []
        current_comments, next_link = self.getItemPerPage(start_url)
        all_comments += current_comments
        while next_link is not None:
            time.sleep(5)
            result = self.getItemPerPage(next_link)
            if result == None:
                next_link = None
                break
                
            current_comments, next_link = result    
            all_comments += current_comments
        
        return all_comments
    
    def getSubItems(self, record_array):
        sub_array = []
        for item in record_array:
            if item['comment_count'] > 0:
                url = self.createCommentURL(item['id'])
                sub_array += self.getAllItems(url)
        return sub_array

    @staticmethod
    def convertToWords(series):
        all_set=set([])
        for curr_tuple in series.iteritems():
            curr_line = rgx.sub(r'[\.]+', ' ', curr_tuple[1].lower())
            clean_line = curr_line.split(' ')
            all_set = all_set.union(set(clean_line))

        return pd.Series(list(all_set))

class FacebookScraper():
    """
        Scrape facebook page in case no permission allowed to get from API
        Using Firefox browser, make sure to install Gecko driver first        
    """
    showCommentPath = "//div[@class='_524d']/a"
    showMorePath = "//a[@class='_5v47 fss']"
    loadMorePath = "//a[@class='UFIPagerLink']"
    showRepliesPath = "//a[@class='UFICommentLink']"

    def __init__(self, url):
        self.url = url

    def runDriver(self):
        self.driver = webdriver.Firefox()
        try:
            self.driver.get(self.url)
        except Exception as e:
            print(e)
            self.driver.quit()
        self.driver.quit()

    def toggleComment(self, xpath):
        try:
            while True:
                elem = self.driver.find_element_by_xpath(xpath)
                elem.click()
                time.sleep(2)
        except exceptions.NoSuchElementException:
            print('No such element')
        else:
            print('other error')

    def convertToSoup(self):
        soup = BeautifulSoup(self.driver.page_source, "lxml")
        comments = soup.find_all(class_="UFICommentActorAndBody")

        usr = [], usrCom = []
        for comment in comments:
            usr.append(comment.find(class_=" UFICommentActorName").text)
            usrCom.append(comment.find(class_="UFICommentBody").text)

        return pd.DataFrame(data={'user': usr, 'comments': usrCom})

# url = r'https://graph.facebook.com/v2.12/361018294015520_1631380266979310?fields=comments%7Bcomment_count%2Cmessage%2Ccreated_time%2Cfrom%7D&access_token=643368762522881|c4bf0124fa9dffe8b824e9b4e4e50ff8'
# result = getAllComments(url)
# df = pd.DataFrame(result)
# df.to_csv(path_or_buf=r'D:/Qayyuum/aliffsyukri1.csv')

fb = FacebookAPIImporter(r'https://www.facebook.com/thesiakapkeli/', r'643368762522881|c4bf0124fa9dffe8b824e9b4e4e50ff8')
fb.setGroupId()
test = fb.getPostList()
ids = [ x['id'] for x in test[0] ]
print(ids)

comment_url = fb.createCommentURL('591441797545512_2044197555603255')
arr = fb.getAllItems(comment_url)
df = pd.DataFrame(arr)
df.to_excel(r'D:/Qayyuum/AirAsia.xlsx')